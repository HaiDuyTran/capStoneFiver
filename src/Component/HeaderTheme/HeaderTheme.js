import React from "react";
import "./HeaderTheme.css";
import UserNav from "./UserNav";
import { Input } from "antd";
import {
  BellOutlined,
  MailOutlined,
  HeartOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Button, Dropdown, Menu, Space } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { localStorageServ } from "../../Service/localStorageService";
import { loginAction } from "../../redux/Actions/userAction";

export default function HeaderTheme() {
  let dispatch = useDispatch();
  const userInfo = useSelector((state) => state.userReducer.userInfo);
  const { Search } = Input;

  const onSearching = (value) => {
    window.location.href = `/taskList/${value}`;
  };

  const handleLogout = () => {
    localStorageServ.user.remove();
    dispatch(loginAction(null));
  };

  const menu = (
    <Menu
      items={[
        {
          key: "1",
          label: (
            <button
              className=" text-xl"
              onClick={() => {
                window.location.href = "/user";
              }}
            >
              Profile
            </button>
          ),
        },
        {
          key: "2",
          label: (
            <button
              className="text-red-500 font-bold text-xl"
              onClick={() => {
                handleLogout();
                window.location.href = "/";
              }}
            >
              Logout
            </button>
          ),
        },
      ]}
    />
  );

  const renderNav = () => {
    if (userInfo === null) {
      return (
        <ul className="flex gap-x-5">
          <li className="nav-1-title">
            <a href="#">Fiverr Business</a>
          </li>
          <li className="nav-title">
            <a href="#">Explore</a>
          </li>
          <li className="nav-title">
            <a href="#">English</a>
          </li>
          <li className="nav-title">
            <a href="#">$USD</a>
          </li>
          <li className="nav-title">
            <a href="#">Become a seller</a>
          </li>
          <li className="nav-title">
            <a href="#">Sign in</a>
          </li>
          <li>
            <button
              className="join-button"
              onClick={() => {
                window.location.href = "/login";
              }}
            >
              Join
            </button>
          </li>
        </ul>
      );
    } else {
      return (
        <ul className="flex gap-x-10 justify-center items-center">
          <li className="nav-1-title">
            <a href="#">
              <BellOutlined />
            </a>
          </li>
          <li className="nav-title">
            <a href="#">
              <MailOutlined />
            </a>
          </li>
          <li className="nav-title">
            <a href="#">
              <HeartOutlined />
            </a>
          </li>
          <li className="nav-title">
            <a href="#">Order</a>
          </li>
          <li className="flex gap-2 justify-center items-center">
            <UserOutlined />
            <Dropdown overlay={menu} placement="bottomLeft">
              <Button
                className="join-button py-2"
                onClick={() => {
                  window.location.href = "/user";
                }}
              >
                {userInfo.user.name}
              </Button>
            </Dropdown>
          </li>
        </ul>
      );
    }
  };

  return (
    <div>
      <div id="Header">
        <header className="fiver-header px-24 py-5">
          <div className="header-row-wrapper">
            <div className="header-row flex gap-x-10 justify-between">
              <div className="fiver-logo flex gap-5 items-center">
                <a href="/" className="site-logo">
                  <svg
                    width={89}
                    height={27}
                    viewBox="0 0 89 27"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <g fill="#404145">
                      <path d="m81.6 13.1h-3.1c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-13.4h-2.5c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-18.4h6v2.8c1-2.2 2.3-2.8 4.3-2.8h7.3v2.8c1-2.2 2.3-2.8 4.3-2.8h2zm-25.2 5.6h-12.4c.3 2.1 1.6 3.2 3.7 3.2 1.6 0 2.7-.7 3.1-1.8l5.3 1.5c-1.3 3.2-4.5 5.1-8.4 5.1-6.5 0-9.5-5.1-9.5-9.5 0-4.3 2.6-9.4 9.1-9.4 6.9 0 9.2 5.2 9.2 9.1 0 .9 0 1.4-.1 1.8zm-5.7-3.5c-.1-1.6-1.3-3-3.3-3-1.9 0-3 .8-3.4 3zm-22.9 11.3h5.2l6.6-18.3h-6l-3.2 10.7-3.2-10.8h-6zm-24.4 0h5.9v-13.4h5.7v13.4h5.9v-18.4h-11.6v-1.1c0-1.2.9-2 2.2-2h3.5v-5h-4.4c-4.3 0-7.2 2.7-7.2 6.6v1.5h-3.4v5h3.4z" />
                    </g>
                    <g fill="#1dbf73">
                      <path d="m85.3 27c2 0 3.7-1.7 3.7-3.7s-1.7-3.7-3.7-3.7-3.7 1.7-3.7 3.7 1.7 3.7 3.7 3.7z" />
                    </g>
                  </svg>
                </a>

                <div className="fiver-header-search">
                  <Search
                    className="rounded-lg relative"
                    placeholder="What service are you looking for today?"
                    allowClear
                    enterButton={<SearchOutlined />}
                    size="large"
                    style={{ width: "450px" }}
                    onSearch={onSearching}
                  />
                </div>
              </div>

              <nav className="fiver-nav">{renderNav()}</nav>
            </div>
          </div>
        </header>
      </div>
      <div>
        <UserNav />
      </div>
    </div>
  );
}
