import {
  GET_COMMENT_BY_TASK,
  GET_DETAIL_TYPE_OF_TASK,
  GET_TASK_LIST_BY_NAME,
  GET_TASK_LIST_BY_TYPE,
  GET_TASK_SELECTED,
  GET_TYPE_OF_TASK,
} from "../Constants/taskConstant";

const initialState = {
  arrTask: [],
  arrTypeOfTask: [],
  typeOfTask: "",
  arrDetailTypeOfTask: [],
  taskSelected: [],
  arrCommentByTask: [],
};

export const taskReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_TASK_LIST_BY_NAME: {
      state.arrTask = payload;
      return { ...state };
    }
    case GET_TASK_LIST_BY_TYPE: {
      state.arrTask = payload;
      state.typeOfTask = payload[0].tenChiTietLoai;
      return { ...state };
    }
    case GET_TYPE_OF_TASK: {
      state.arrTypeOfTask = payload;
      return { ...state };
    }

    case GET_DETAIL_TYPE_OF_TASK: {
      state.arrDetailTypeOfTask = payload;
      return { ...state };
    }

    case GET_TASK_SELECTED: {
      state.taskSelected = payload;
      return { ...state };
    }

    case GET_COMMENT_BY_TASK: {
      state.arrCommentByTask = payload;
      return { ...state };
    }

    default:
      return { ...state };
  }
};
