import { combineReducers } from "redux";
import { taskReducer } from "./taskReducer";
import { userReducer } from "./userReducer";
export let rootReducer = combineReducers({
  taskReducer,
  userReducer,
});
