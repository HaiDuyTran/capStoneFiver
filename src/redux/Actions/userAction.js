import { LOGIN, REGISTER } from "../Constants/userConstant";

export let loginAction = (dataLogin) => {
  return {
    type: LOGIN,
    payload: dataLogin,
  };
};
export let registerAction = (dataRegister) => {
  return {
    type: REGISTER,
    payload: dataRegister,
  };
};
