import { message } from "antd";
import { taskService } from "../../Service/taskService";
import {
  GET_COMMENT_BY_TASK,
  GET_DETAIL_TYPE_OF_TASK,
  GET_TASK_LIST_BY_NAME,
  GET_TASK_LIST_BY_TYPE,
  GET_TASK_SELECTED,
  GET_TYPE_OF_TASK,
} from "../Constants/taskConstant";

export const getTaskListByNameAction = (tenCongViec) => {
  return (dispatch) => {
    taskService
      .getTaskListByName(tenCongViec)
      .then((res) => {
        dispatch({
          type: GET_TASK_LIST_BY_NAME,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const getTaskListByTypeAction = (maChiTietLoai) => {
  return (dispatch) => {
    taskService
      .getTaskListByType(maChiTietLoai)
      .then((res) => {
        dispatch({
          type: GET_TASK_LIST_BY_TYPE,
          payload: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

export const getTypeOfTaskAction = () => {
  return (dispatch) => {
    taskService
      .getTypeOfTask()
      .then((res) => {
        dispatch({
          type: GET_TYPE_OF_TASK,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const getDetailTypeOfTaskAction = (maLoaiCongViec) => {
  return (dispatch) => {
    taskService
      .getDetailTypeOfTask(maLoaiCongViec)
      .then((res) => {
        dispatch({
          type: GET_DETAIL_TYPE_OF_TASK,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const getTaskSelectedAction = (maCongViec) => {
  return (dispatch) => {
    taskService
      .getTaskSelected(maCongViec)
      .then((res) => {
        dispatch({
          type: GET_TASK_SELECTED,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const getCommentByTaskAction = (maCongViec) => {
  return (dispatch) => {
    taskService
      .getCommentByTask(maCongViec)
      .then((res) => {
        dispatch({
          type: GET_COMMENT_BY_TASK,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const postCommentAction = (dataComment) => {
  return (dispatch) => {
    taskService
      .postComment(dataComment)
      .then((res) => {
        message.success("Gửi bình luận thành công");
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

export const rentTaskAction = (dataThueCongViec) => {
  return (dispatch) => {
    taskService
      .rentTask(dataThueCongViec)
      .then((res) => {
        message.success(res.data.message);
      })
      .catch((err) => {});
  };
};
