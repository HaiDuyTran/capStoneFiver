import React from "react";
import "./UserInforPage.css";
export default function UserInforPage() {
  return (
    <div
      className="px-auto grid grid-cols-3 gap-20"
      style={{ backgroundColor: "#f7f7f7", padding: "40px" }}
    >
      <div className="info-column col-span-1">Info</div>
      <div className="active-column col-span-2">Active</div>
    </div>
  );
}
