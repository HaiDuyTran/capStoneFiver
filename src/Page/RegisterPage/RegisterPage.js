import React from "react";
import { Button, Form, Input, message } from "antd";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { userService } from "../../Service/userService";
import { registerAction } from "../../redux/Actions/userAction";
import { localStorageServ } from "../../Service/localStorageService";
export default function RegisterPage() {
  let dispatch = useDispatch();
  let history = useNavigate();

  const onFinish = (values) => {
    userService
      .postRegister(values)
      .then((res) => {
        message.success("Đăng ký thành công");

        dispatch(registerAction(res.data.content));

        localStorageServ.user.set(res.data.content);
        setTimeout(() => {
          // chuyển trang
          history("/login");
        }, 1000);
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };
  /* eslint-disable no-template-curly-in-string */

  const validateMessages = {
    required: "${label} is required!",
  };
  /* eslint-enable no-template-curly-in-string */

  return (
    <div className="register_page bg-green-500 w-screen h-screen p-10">
      <div className="container rounded-xl mx-auto p-10 bg-white flex ">
        <div className="w-1/2">
          <Form
            {...layout}
            name="nest-messages"
            onFinish={onFinish}
            validateMessages={validateMessages}
          >
            <Form.Item
              name="taiKhoan"
              label="Tài khoản"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="matKhau"
              label="Mật khẩu"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mật khẩu!",
                },
              ]}
              hasFeedback
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              name="xacNhan"
              label="Nhập lại mật khẩu"
              dependencies={["matKhau"]}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập lại mật khẩu!",
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue("matKhau") === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(
                      new Error("Mật khẩu nhập lại không trùng nhau !")
                    );
                  },
                }),
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              name="name"
              label="Họ Tên"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="email"
              label="Email"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="soDt"
              label="Số điện thoại"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
              <Button type="primary" htmlType="submit">
                Đăng ký
              </Button>
            </Form.Item>
          </Form>
        </div>
        <button
          className="absolute -top-4 -right-4 font-bold text-2xl bg-gray-500 rounded-full px-2"
          onClick={() => {
            window.location.href = "/";
          }}
        >
          X
        </button>
      </div>
    </div>
  );
}
