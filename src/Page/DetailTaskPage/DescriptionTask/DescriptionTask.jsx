import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getTaskListByTypeAction } from "../../../redux/Actions/taskAction";

export default function DescriptionTask(props) {
  const dispatch = useDispatch();
  const id = props.id;

  useEffect(() => {
    if (props.maChiTietLoaiCongViec != null) {
      dispatch(getTaskListByTypeAction(props.maChiTietLoaiCongViec));
    }
  }, [props.maChiTietLoaiCongViec]);

  const { arrTask } = useSelector((state) => state.taskReducer);

  if (arrTask.length != 0) {
    const task = arrTask.filter((task) => {
      return task.id == id;
    });
    const { congViec } = {
      ...task[0],
    };

    const { moTa, moTaNgan } = {
      ...congViec,
    };

    return (
      <div>
        <h1 className="title my-2 text-xl font-medium">About this Gig</h1>

        <p className="text-lg font-medium " style={{ whiteSpace: "pre-line" }}>
          {moTa}
        </p>
        <br />
        <p
          className="text-md text-gray-400 font-medium"
          style={{ whiteSpace: "pre-line" }}
        >
          {moTaNgan}
        </p>
      </div>
    );
  }
}
