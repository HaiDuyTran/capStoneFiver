import "./CommentLoading.css";
import React, { useEffect, createElement, useState } from "react";
import { useFormik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import {
  getCommentByTaskAction,
  postCommentAction,
} from "../../../redux/Actions/taskAction";
import {
  DislikeFilled,
  DislikeOutlined,
  LikeFilled,
  LikeOutlined,
} from "@ant-design/icons";
import { Avatar, Comment, Tooltip, Form, Input, message } from "antd";
import { localStorageServ } from "../../../Service/localStorageService";
import moment from "moment";

export default function CommentLoading(props) {
  // tạo layout comment
  const { TextArea } = Input;
  const [likes, setLikes] = useState(0);
  const [dislikes, setDislikes] = useState(0);
  const [action, setAction] = useState(null);
  const like = () => {
    setLikes(1);
    setDislikes(0);
    setAction("liked");
  };
  const dislike = () => {
    setLikes(0);
    setDislikes(1);
    setAction("disliked");
  };
  const actions = [
    <Tooltip key="comment-basic-like" title="Like">
      <span onClick={like}>
        {createElement(action === "liked" ? LikeFilled : LikeOutlined)}
        <span className="comment-action">{likes}</span>
      </span>
    </Tooltip>,
    <Tooltip key="comment-basic-dislike" title="Dislike">
      <span onClick={dislike}>
        {React.createElement(
          action === "disliked" ? DislikeFilled : DislikeOutlined
        )}
        <span className="comment-action">{dislikes}</span>
      </span>
    </Tooltip>,
    <span key="comment-basic-reply-to">Reply to</span>,
  ];
  // tạo layout comment
  const dispatch = useDispatch();

  let maCongViec = props.id;

  useEffect(() => {
    dispatch(getCommentByTaskAction(maCongViec));
  }, []);

  const { arrCommentByTask } = useSelector((state) => state.taskReducer);

  const renderComment = () => {
    if (arrCommentByTask?.length == 0) {
      return <div>There are no comment yet!!</div>;
    } else
      return arrCommentByTask?.map((comment, index) => {
        return (
          <div>
            <Comment
              key={index}
              actions={actions}
              author={comment.tenNguoiBinhLuan}
              avatar={<Avatar src={comment.avatar} alt="" />}
              content={comment.noiDung}
              datetime={<Tooltip>{comment.ngayBinhLuan}</Tooltip>}
            />
          </div>
        );
      });
  };

  const formik = useFormik({
    initialValues: {
      id: moment,
      maNguoiBinhLuan: localStorageServ.user.get()?.user.id,
      maCongViec: maCongViec,
      ngayBinhLuan: moment().format("DD/MM/YYYY"),
      noiDung: "",
      saoBinhLuan: 0,
    },
    onSubmit: (values) => {
      if (localStorageServ.user.get() === null) {
        message.warning("vui lòng đăng nhập trước khi bình luận");

        setTimeout(() => {
          window.location.href = "/login";
        }, 1000);
      } else {
        dispatch(postCommentAction(values));

        setTimeout(() => {
          dispatch(getCommentByTaskAction(maCongViec));
        }, 1000);
      }
    },
  });

  return (
    <div>
      {renderComment()}
      <div className="send_comment mt-4">
        <Form onSubmitCapture={formik.handleSubmit}>
          <Form.Item
            id="noiDung"
            name="noiDung"
            initialValue=""
            onChange={formik.handleChange}
            value={formik.values.noiDung}
          >
            <TextArea rows={4} />
          </Form.Item>
          <Form.Item>
            <button
              type="submit"
              className="py-2 px-4 bg-blue-500 text-white font-semibold hover:bg-blue-300 rounded"
            >
              Submit now
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
