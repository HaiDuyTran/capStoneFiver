import { Breadcrumb, Rate, Menu, Progress } from "antd";
import React from "react";
import {
  AppstoreOutlined,
  Html5Outlined,
  MailOutlined,
  SettingOutlined,
  StarFilled,
} from "@ant-design/icons";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { getTaskSelectedAction } from "../../redux/Actions/taskAction";
import "./DetailTaskPage.css";
import CommentLoading from "./CommentLoading/CommentLoading";
import RentTask from "./RentTask/RentTask";
import DescriptionTask from "./DescriptionTask/DescriptionTask";

export default function DetailTaskPage() {
  const dispatch = useDispatch();

  const { id } = useParams();

  useEffect(() => {
    dispatch(getTaskSelectedAction(id));
  }, []);

  const { taskSelected } = useSelector((state) => state.taskReducer);

  const {
    congViec,
    avatar,
    tenNguoiTao,
    tenLoaiCongViec,
    tenNhomChiTietLoai,
    tenChiTietLoai,
  } = {
    ...taskSelected[0],
  };

  const { tenCongViec, saoCongViec, danhGia, hinhAnh, maChiTietLoaiCongViec } =
    {
      ...congViec,
    };

  // drop menu ANTD
  function getItem(label, key, icon, children, type) {
    return {
      key,
      children,
      label,
      type,
      icon,
    };
  }

  const items = [
    getItem(
      "Do you provide regular updates on order?",
      "sub1",
      <MailOutlined />,
      []
    ),
    getItem(
      "How do you guarantee product quality and rellablity?",
      "sub2",
      <AppstoreOutlined />,
      []
    ),
    getItem(
      "Do you give Post-development support?",
      "sub4",
      <SettingOutlined />,
      []
    ),
    getItem("Do you convert PSD to HTML?", "sub3", <Html5Outlined />, []),
  ];

  const onClick = (e) => {
    console.log("click ", e);
  };
  // drop menu ANTD

  return (
    <div>
      <div className="breadcrumb_area px-24 my-2">
        <Breadcrumb separator=">">
          <Breadcrumb.Item href="">Home</Breadcrumb.Item>
          <Breadcrumb.Item href="">{tenLoaiCongViec}</Breadcrumb.Item>
          <Breadcrumb.Item href="">{tenNhomChiTietLoai}</Breadcrumb.Item>
          <Breadcrumb.Item>{tenChiTietLoai}</Breadcrumb.Item>
        </Breadcrumb>
      </div>

      <div className="taskDetailContent px-24 grid grid-cols-12">
        <div className="left col-span-7">
          {/* title */}
          <h1 className="text-3xl font-semibold">{tenCongViec}</h1>
          {/* thong tin chung */}
          <div className="flex flex-row my-5 items-center gap-3">
            <img
              style={{ width: "30px", height: "30px", borderRadius: "15px" }}
              src={avatar}
              alt=""
            />
            <h2 className="font-semibold text-base">{tenNguoiTao}</h2>

            <span className="text-lg font-semibold">|</span>

            <Rate className="pb-1" disabled value={saoCongViec} />

            <h1 className="font-semibold text-xl" style={{ color: "#fadb14" }}>
              {saoCongViec}
            </h1>

            <h3 className="font-semibold text-xl">({danhGia}) </h3>

            <span className="text-lg font-semibold">|</span>

            <h3 className="text-xl text-gray-500">
              {parseInt(danhGia / 5)} Orders in Queue
            </h3>
          </div>
          {/* hình ảnh mô ta task*/}
          <div className="w-full my-5">
            <img
              style={{ height: "400px" }}
              className="w-full object-cover"
              src={hinhAnh}
              alt=""
            />
          </div>
          {/* mô tả chi tiết task */}
          <div className="description my-5">
            <DescriptionTask
              id={id}
              maChiTietLoaiCongViec={maChiTietLoaiCongViec}
            />
          </div>
          {/* thông tin ng tạo task */}
          <div className="seller_info my-5">
            <h1 className="title my-2 text-xl font-medium">About the Seller</h1>

            <div className="seller_info_content flex flex-row">
              <img src={avatar} alt="" />

              <div className="info ml-5">
                <Rate disabled value={saoCongViec} />
                <p className="text-xl mb-2 font-semibold">{tenNguoiTao}</p>
                <button className="px-6 py-2 rounded-lg text-white font-semibold hover:bg-blue-300 bg-blue-500">
                  Contact me!!!
                </button>
              </div>
            </div>
          </div>
          {/* FAQ - data cứng */}
          <div className="FAQ my-5">
            <h1 className="title my-2 text-xl font-medium">FAQ</h1>

            <Menu
              onClick={onClick}
              style={{
                width: "100%",
              }}
              defaultSelectedKeys={["1"]}
              defaultOpenKeys={["sub1"]}
              mode="inline"
              items={items}
            />
          </div>
          {/* review bar - data cứng */}
          <div className="review my-5">
            <div className="title flex flex-row items-center gap-4">
              <h1 className="title my-2 text-md font-bold">
                {danhGia} Reviewer
              </h1>

              <Rate className="pb-1" disabled value={saoCongViec} />

              <h1
                className="font-semibold text-xl"
                style={{ color: "#fadb14" }}
              >
                {saoCongViec}
              </h1>
            </div>

            <div className="review_bar w-2/3">
              <div className="bar-items flex flex-row gap-4 items-center">
                <h1 className="font-semibold">5</h1>
                <StarFilled style={{ color: "#fadb14" }} />
                <Progress showInfo={false} strokeColor="#fadb14" percent={70} />
                <h1 className="font-semibold"> {parseInt(danhGia * 0.7)} </h1>
              </div>
              <div className="bar-items flex flex-row gap-4 items-center">
                <h1 className="font-semibold">4</h1>
                <StarFilled style={{ color: "#fadb14" }} />
                <Progress showInfo={false} strokeColor="#fadb14" percent={20} />
                <h1 className="font-semibold"> {parseInt(danhGia * 0.2)} </h1>
              </div>
              <div className="bar-items flex flex-row gap-4 items-center">
                <h1 className="font-semibold">3</h1>
                <StarFilled style={{ color: "#fadb14" }} />
                <Progress showInfo={false} strokeColor="#fadb14" percent={10} />
                <h1 className="font-semibold"> {parseInt(danhGia * 0.1)} </h1>
              </div>
              <div className="bar-items flex flex-row gap-4 items-center">
                <h1 className="font-semibold">2</h1>
                <StarFilled style={{ color: "#fadb14" }} />
                <Progress showInfo={false} strokeColor="#fadb14" percent={0} />
                <h1 className="font-semibold"> {parseInt(danhGia * 0)} </h1>
              </div>
              <div className="bar-items flex flex-row gap-4 items-center">
                <h1 className="font-semibold">1</h1>
                <StarFilled style={{ color: "#fadb14" }} />
                <Progress showInfo={false} strokeColor="#fadb14" percent={0} />
                <h1 className="font-semibold"> {parseInt(danhGia * 0)} </h1>
              </div>
            </div>
          </div>
          {/* Load & send comment */}
          <div className="comment my-5">
            <h1 className="title my-2 text-md font-bold">Top comment</h1>

            <CommentLoading id={id} />
          </div>
        </div>

        <div className="right col-span-5 p-10 relative shadow-none">
          {/* thuê công việc */}
          <div className="sticky">
            <RentTask id={id} />
          </div>
        </div>
      </div>
    </div>
  );
}
