import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { getDetailTypeOfTaskAction } from "../../redux/Actions/taskAction";
import { PlayCircleFilled } from "@ant-design/icons";
import "./TypeOfTask.css";
export default function TypeOfTaskPage() {
  let dispatch = useDispatch();
  let { maLoaiCongViec } = useParams();
  let { arrDetailTypeOfTask } = useSelector((state) => state.taskReducer);
  useEffect(() => {
    dispatch(getDetailTypeOfTaskAction(maLoaiCongViec));
  }, []);

  let renderBanner = () => {
    return arrDetailTypeOfTask.map((item) => {
      return (
        <div className="banner-content">
          <p key={item.id} className="text-3xl font-bold">
            {item.tenLoaiCongViec}
          </p>
          <p className="text-2xl mt-2">Designs to make you stand out.</p>
          <div className="play-button">
            <div className="play mt-2">
              <PlayCircleFilled />
              <p className="ml-5">How Fiver Works</p>
            </div>
          </div>
        </div>
      );
    });
  };
  let renderTopic = () => {
    return arrDetailTypeOfTask.map((item) => {
      return (
        <p key={item.id} className="text-3xl font-bold">
          {item.tenLoaiCongViec}
        </p>
      );
    });
  };

  let renderTypeOfTask = () => {
    return arrDetailTypeOfTask.map((item) => {
      return item.dsNhomChiTietLoai.map((nhomChiTietLoai) => {
        return (
          <div className="type-task mt-5" key={nhomChiTietLoai.id}>
            <img
              src={nhomChiTietLoai.hinhAnh}
              style={{ width: "100%" }}
              alt=""
            />
            <ul>
              <li className="font-bold text-xl mt-5">
                {nhomChiTietLoai.tenNhom}
              </li>
              {nhomChiTietLoai.dsChiTietLoai.map((chiTietLoai) => {
                return (
                  <li className="chiTietLoai mt-5" key={chiTietLoai.id}>
                    <p>{chiTietLoai.tenChiTiet}</p>
                  </li>
                );
              })}
            </ul>
          </div>
        );
      });
    });
  };

  return (
    <div className="container mx-auto mt-10 mb-24">
      <div className="banner text-center">{renderBanner()}</div>
      <div className="bucket mt-10">
        {renderTopic()}
        <div className="grid grid-cols-4 gap-10 mt-10 mb-10">
          {renderTypeOfTask()}
        </div>
      </div>
      <div className="relate-content mt-10">
        <h1 className="text-4xl font-bold text-center">
          Services Related To Digital Marketing
        </h1>
        <ul className="content mt-10 flex">
          <li className="relate-item">
            <p>SEO services</p>
          </li>
          <li className="relate-item">
            <p>Music promotion</p>
          </li>
          <li className="relate-item">
            <p>Social media marketing</p>
          </li>
          <li className="relate-item">
            <p>Video marketing</p>
          </li>
          <li className="relate-item">
            <p>Social Content</p>
          </li>
          <li className="relate-item">
            <p>Influencer marketing</p>
          </li>
          <li className="relate-item">
            <p>Spotify music promotion</p>
          </li>
          <li className="relate-item">
            <p>Shoutouts & Promotion</p>
          </li>
          <li className="relate-item">
            <p>Youtube marketing</p>
          </li>
        </ul>
      </div>
    </div>
  );
}
