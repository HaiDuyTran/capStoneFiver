import React, { useEffect, useState } from "react";
import { Switch } from "antd";
import { DownOutlined } from "@ant-design/icons";
import { Card } from "antd";
import { NavLink, useParams } from "react-router-dom";
import { HeartFilled } from "@ant-design/icons";
import "./ListTaskPage.css";
import { useDispatch, useSelector } from "react-redux";
import {
  getTaskListByNameAction,
  getTaskListByTypeAction,
} from "../../redux/Actions/taskAction";

export default function ListTaskPage() {
  let dispatch = useDispatch();
  const { tenCongViec, maChiTietLoai } = useParams();

  const { arrTask, typeOfTask } = useSelector((state) => state.taskReducer);

  const goToTaskDetail = (id) => {
    window.location.href = `/detailTask/${id}`;
  };
  
  useEffect(() => {
    dispatch(getTaskListByNameAction(tenCongViec));
  }, []);

  useEffect(() => {
    dispatch(getTaskListByTypeAction(maChiTietLoai));
  });
  

  const renderListTask = () => {
    return arrTask.map((task, index) => {
      return (
        <div key={index} className="task">
          <Card
            onClick={() => {
              goToTaskDetail(task.congViec.id);
            }}
            hoverable
            style={{ width: 260 }}
          >
            <NavLink>
              <img alt="example" src={task.congViec.hinhAnh} />
              <h2 className="text-black font-bold text-xl ">
                {task.congViec.tenCongViec}
              </h2>
            </NavLink>
            <div className="user-info">
              <img src={task.avatar} alt="" width={50} />
              <p>{task.tenNguoiTao}</p>
            </div>
            <div className="content-info">
              <span className="rating flex">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 1792 1792"
                  width={15}
                  height={15}
                >
                  <path
                    fill="currentColor"
                    d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                  />
                </svg>
                {task.congViec.saoCongViec}
              </span>
              <span className="danhGia">({task.congViec.danhGia})</span>
            </div>

            <div className="price-wrapper">
              <div className="heart">
                <HeartFilled />
              </div>
              <div className="price flex">
                <div className="price-text">STARTING AT </div>
                <div className="price-number">${task.congViec.giaTien}</div>
              </div>
            </div>
          </Card>
        </div>
      );
    });
  };

  const renderResult = () => {
    if (tenCongViec !== undefined) {
      return "Result for " + `"${tenCongViec}"`;
    } else {
      return `${typeOfTask}`;
    }
  };

  return (
    <div className="px-24 mt-5">
      <div className="search-header ">
        <p className="title text-black font-bold text-4xl p-2 ml-5">
          {renderResult()}
        </p>
      </div>

      <div className="sticky">
        <div className="top-bar grid grid-cols-8">
          <div className="top-filter col-span-5">
            <div className="filter-item flex ">
              <button>
                Category
                <DownOutlined
                  style={{
                    fontSize: "12px",
                    color: "#74767e",
                    marginLeft: "10px",
                  }}
                />
              </button>
              <button>
                Service Options
                <DownOutlined
                  style={{
                    fontSize: "12px",
                    color: "#74767e",
                    marginLeft: "10px",
                  }}
                />
              </button>
              <button>
                Seller Details
                <DownOutlined
                  style={{
                    fontSize: "12px",
                    color: "#74767e",
                    marginLeft: "10px",
                  }}
                />
              </button>
              <button>
                Budget
                <DownOutlined
                  style={{
                    fontSize: "12px",
                    color: "#74767e",
                    marginLeft: "10px",
                  }}
                />
              </button>
              <button>
                Delivery Time
                <DownOutlined
                  style={{
                    fontSize: "12px",
                    color: "#74767e",
                    marginLeft: "10px",
                  }}
                />
              </button>
            </div>
          </div>

          <div className="top-switch col-span-3 flex items-center justify-around">
            <div className="switch-item">
              <Switch style={{ backgroundColor: "#dadbdd" }}></Switch>
              <p className="pl-2">Pro services</p>
            </div>

            <div className="switch-item">
              <Switch style={{ backgroundColor: "#dadbdd" }}></Switch>
              <p className="pl-2"> Local Sellers</p>
            </div>

            <div className="switch-item">
              <Switch style={{ backgroundColor: "#dadbdd" }}></Switch>
              <p className="pl-2">Online Sellers</p>
            </div>
          </div>
        </div>
      </div>

      <div className="listTask pt-5 grid grid-cols-4 gap-20">
        {renderListTask()}
      </div>
    </div>
  );
}
