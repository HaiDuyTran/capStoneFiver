import React from "react";
import Slider from "react-slick";
import "./HomePage.css";
import { CheckCircleOutlined, SearchOutlined } from "@ant-design/icons";
import { Card, Carousel, Input } from "antd";
import { useDispatch } from "react-redux";

export default function HomePage() {
  const dispatch = useDispatch();
  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div>
        <div
          className={className}
          style={{ ...style, display: "block" }}
          onClick={onClick}
        />
      </div>
    );
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block" }}
        onClick={onClick}
      />
    );
  }

  const settings = {
    centerMode: true,
    infinite: true,
    centerPadding: "0px",
    slidesToShow: 1,
    speed: 800,
    slidesPerRow: 5,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  const settingReview = {
    centerMode: true,
    infinite: true,
    centerPadding: "0px",
    slidesToShow: 1,
    speed: 800,
    slidesPerRow: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  const { Search } = Input;

  const onSearching = (value) => {
    window.location.href = `/taskList/${value}`;
  };
  return (
    <div>
      <div className="carousel relative">
        <div className="onSearch absolute">
          <h1 className=" text-white font-bold mb-4">
            Find the perfect <i className="font-medium">freelancer</i> <br />{" "}
            services for your business
          </h1>

          <Search
            className="rounded-lg"
            placeholder="Try: Building mobile app "
            allowClear
            enterButton={<SearchOutlined />}
            size="large"
            style={{ width: "70%", height: "50px" }}
            onSearch={onSearching}
          />
        </div>

        <Carousel effect="fade" autoplay dots={false}>
          <div className="relative">
            <Card
              style={{ width: "100%" }}
              cover={
                <img
                  alt="example"
                  src="https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/bb5958e41c91bb37f4afe2a318b71599-1599344049983/bg-hero-1-1792-x1.png"
                />
              }
            >
              <div className="descriptionImg absolute bottom-5 right-24 text-white ">
                Andrea, <b>Fasion Designer</b>
              </div>
            </Card>
          </div>

          <div className="relative">
            <Card
              style={{ width: "100%" }}
              cover={
                <img
                  alt="example"
                  src="https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/2413b8415dda9dbd7756d02cb87cd4b1-1599595203045/bg-hero-2-1792-x1.png"
                />
              }
            >
              <div className="descriptionImg absolute bottom-5 right-24 text-white">
                Moon, <b>Marketing Expert</b>
              </div>
            </Card>
          </div>

          <div className="relative">
            <Card
              style={{ width: "100%" }}
              cover={
                <img
                  src="https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/d14871e2d118f46db2c18ad882619ea8-1599835783966/bg-hero-3-1792-x1.png"
                  alt="example"
                />
              }
            >
              <div className="descriptionImg absolute bottom-5 right-24 text-white">
                Ritika, <b>Shoemaker & Designer</b>
              </div>
            </Card>
          </div>

          <div className="relative">
            <Card
              style={{ width: "100%" }}
              cover={
                <img
                  src="https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/93085acc959671e9e9e77f3ca8147f82-1599427734108/bg-hero-4-1792-x1.png"
                  alt="example"
                />
              }
            >
              <div className="descriptionImg absolute bottom-5 right-24 text-white">
                Zach, <b>Bar owner</b>
              </div>
            </Card>
          </div>

          <div className="relative">
            <Card
              style={{ width: "100%" }}
              cover={
                <img
                  alt="example"
                  src="https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/bb5958e41c91bb37f4afe2a318b71599-1599344049970/bg-hero-5-1792-x1.png"
                />
              }
            >
              <div className="descriptionImg absolute bottom-5 right-24 text-white">
                Gabrielle, <b>Video Editor</b>
              </div>
            </Card>
          </div>
        </Carousel>
      </div>

      {/* DATA CỨNG THEO ĐỀ BÀI */}
      <div className="trustBy">
        <div className="brand container mx-auto flex justify-around p-10">
          <div className="grid grid-cols-6 gap-10">
            <div className="flex items-center">
              <h3 className="font-medium text-xl opacity-30">Trusted by:</h3>
            </div>
            <div>
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/facebook.31d5f92.png"
                alt="facebook"
              />
            </div>
            <div>
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/google.517da09.png"
                alt="google"
              />
            </div>
            <div>
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/netflix.e3ad953.png"
                alt="netflix"
              />
            </div>
            <div>
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/pandg.8b7310b.png"
                alt="P&G"
              />
            </div>
            <div>
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/paypal.ec56157.png"
                alt="paypal"
              />
            </div>
          </div>
        </div>
      </div>

      <div className="container px-24 py-20">
        <div className="slider_title">
          <h1 className="text-4xl font-bold mb-5">
            Popular professional services
          </h1>

          <Slider {...settings}>
            <div className="swiper-slide relative">
              <div className="slide-content">
                <img
                  src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png"
                  alt=""
                />
              </div>

              <div
                style={{ top: "20px", left: "20px" }}
                className="slider_name text-white absolute"
              >
                <p className="font-medium">Build your brand</p>
                <b className="text-2xl">Logo Design</b>
              </div>
            </div>

            <div className="swiper-slide relative">
              <div className="slide-content">
                <img
                  src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png"
                  alt=""
                />
              </div>

              <div
                style={{ top: "20px", left: "20px" }}
                className="slider_name text-white absolute"
              >
                <p className="font-medium">Customize your site</p>
                <b className="text-2xl">WordPress</b>
              </div>
            </div>

            <div className="swiper-slide relative">
              <div className="slide-content">
                <img
                  src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png"
                  alt=""
                />
              </div>

              <div
                style={{ top: "20px", left: "20px" }}
                className="slider_name text-white absolute"
              >
                <p className="font-medium">Share your message</p>
                <b className="text-2xl">Voice Over</b>
              </div>
            </div>

            <div className="swiper-slide relative">
              <div className="slide-content">
                <img
                  src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png"
                  alt=""
                />
              </div>

              <div
                style={{ top: "20px", left: "20px" }}
                className="slider_name text-white absolute"
              >
                <p className="font-medium">Engage your audience</p>
                <b className="text-2xl">Video Explainer</b>
              </div>
            </div>

            <div className="swiper-slide relative">
              <div className="slide-content">
                <img
                  src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png"
                  alt=""
                />
              </div>

              <div
                style={{ top: "20px", left: "20px" }}
                className="slider_name text-white absolute"
              >
                <p className="font-medium">Reach more customers</p>
                <b className="text-2xl">Social media</b>
              </div>
            </div>

            <div className="swiper-slide relative">
              <div className="slide-content">
                <img
                  src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png"
                  alt=""
                />
              </div>

              <div
                style={{ top: "20px", left: "20px" }}
                className="slider_name text-white absolute"
              >
                <p className="font-medium">Unlock growth online</p>
                <b className="text-2xl">SEO</b>
              </div>
            </div>

            <div className="swiper-slide relative">
              <div className="slide-content">
                <img
                  src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png"
                  alt=""
                />
              </div>

              <div
                style={{ top: "20px", left: "20px" }}
                className="slider_name text-white absolute"
              >
                <p className="font-medium">Color your dream</p>
                <b className="text-2xl">ILLustration</b>
              </div>
            </div>

            <div className="swiper-slide relative">
              <div className="slide-content">
                <img
                  src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png"
                  alt=""
                />
              </div>

              <div
                style={{ top: "20px", left: "20px" }}
                className="slider_name text-white absolute"
              >
                <p className="font-medium">Go global</p>
                <b className="text-2xl">Translation</b>
              </div>
            </div>

            <div className="swiper-slide relative">
              <div className="slide-content">
                <img
                  src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png"
                  alt=""
                />
              </div>

              <div
                style={{ top: "20px", left: "20px" }}
                className="slider_name text-white absolute"
              >
                <p className="font-medium">Learn your business</p>
                <b className="text-2xl">Data Entry</b>
              </div>
            </div>

            <div className="swiper-slide relative">
              <div className="slide-content">
                <img
                  src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png"
                  alt=""
                />
              </div>

              <div
                style={{ top: "20px", left: "20px" }}
                className="slider_name text-white absolute"
              >
                <p className="font-medium">Showcase your story</p>
                <b className="text-2xl">Book Covers</b>
              </div>
            </div>
          </Slider>
        </div>
      </div>

      <div className="benefit grid grid-cols-11 px-24 py-20">
        <div className="left col-span-5">
          <h1 className="text-4xl font-bold mb-5">
            A whole world of freelance <br /> talent at your fingertips
          </h1>

          <ul>
            <li className="text-xl mb-5">
              <div className="flex items-center font-bold">
                <CheckCircleOutlined />{" "}
                <p className="ml-2">The best for every budget</p>
              </div>
              <h2 className="mt-2">
                Find high-quality services at every price point. No <br />{" "}
                hourly rates, just project-based pricing.
              </h2>
            </li>

            <li className="text-xl mb-5">
              <div className="flex items-center font-bold">
                <CheckCircleOutlined />{" "}
                <p className="ml-2">Quality work done quickly</p>
              </div>
              <h2 className="mt-2">
                Find the right freelancer to begin working on your <br />{" "}
                project within minutes.
              </h2>
            </li>

            <li className="text-xl mb-5">
              <div className="flex items-center font-bold">
                <CheckCircleOutlined />{" "}
                <p className="ml-2">Protected payments, every time</p>
              </div>
              <h2 className="mt-2">
                Always know what you'll pay upfront. Your payment <br /> isn't
                released until you approve the work.
              </h2>
            </li>

            <li className="text-xl mb-5">
              <div className="flex items-center font-bold">
                <CheckCircleOutlined /> <p className="ml-2">24/7 support</p>
              </div>
              <h2 className="mt-2">
                Questions? Our round-the-clock support team <br /> is available
                to help anytime, anywhere.
              </h2>
            </li>
          </ul>
        </div>

        <div className="right col-span-6  relative">
          <img
            style={{ width: "100%" }}
            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_700,dpr_1.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png"
            alt=""
          />
        </div>
      </div>

      <div className="marketplace px-24 py-20">
        <div className="marketplace_title">
          <h1 className="text-4xl font-bold mb-7">Explore the marketplace</h1>
        </div>

        <div className="marketplace_content grid justify-items-center grid-cols-5 gap-y-20 ">
          <div className="item">
            <img
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/graphics-design.d32a2f8.svg"
              alt=""
            />

            <hr />

            <p className="mt-2 font-semibold text-xl">Graphic & Design</p>
          </div>

          <div className="item">
            <img
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/online-marketing.74e221b.svg"
              alt=""
            />

            <hr />

            <p className="mt-2 font-semibold text-xl">Digital Marketing</p>
          </div>

          <div className="item">
            <img
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/video-animation.f0d9d71.svg"
              alt=""
            />

            <hr />

            <p className="mt-2 font-semibold text-xl">Video & Animation</p>
          </div>

          <div className="item">
            <img
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/writing-translation.32ebe2e.svg"
              alt=""
            />

            <hr />

            <p className="mt-2 font-semibold text-xl">Writing & Translation</p>
          </div>

          <div className="item">
            <img
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/music-audio.320af20.svg"
              alt=""
            />

            <hr />

            <p className="mt-2 font-semibold text-xl">Music & Audio</p>
          </div>

          <div className="item">
            <img
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/programming.9362366.svg"
              alt=""
            />

            <hr />

            <p className="mt-2 font-semibold text-xl">Programing & Tech</p>
          </div>

          <div className="item">
            <img
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/lifestyle.745b575.svg"
              alt=""
            />

            <hr />

            <p className="mt-2 font-semibold text-xl">LifeStyle</p>
          </div>

          <div className="item">
            <img
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/business.bbdf319.svg"
              alt=""
            />

            <hr />

            <p className="mt-2 font-semibold text-xl">Business</p>
          </div>

          <div className="item">
            <img
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/data.718910f.svg"
              alt=""
            />

            <hr />

            <p className="mt-2 font-semibold text-xl">Data Analyst</p>
          </div>
        </div>
      </div>

      <div className="review px-24 py-20">
        <Slider {...settingReview}>
          <div className="slider_item">
            <div className="grid grid-cols-12">
              <div className="left col-span-5">
                <img
                  src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg"
                  alt=""
                />
              </div>

              <div className="right col-span-7 px-20">
                <div className="flex items-center mb-5">
                  <p className="text-2xl">
                    Caitlin Tormey, Chief Commercial Officer
                  </p>

                  <span className="text-2xl mx-3">|</span>

                  <p className="text-2xl font-bold">NAADAM</p>
                </div>

                <i className="text-4xl font-semibold">
                  "We've used Fiverr for Shopify web development, graphic
                  design, and backend web development. Working with Fiverr makes
                  my job a little easier every day."
                </i>
              </div>
            </div>
          </div>

          <div className="slider_item">
            <div className="grid grid-cols-12">
              <div className="left col-span-5">
                <img
                  src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg"
                  alt=""
                />
              </div>

              <div className="right col-span-7 px-20">
                <div className="flex items-center mb-5">
                  <p className="text-2xl">Kay Kim, Co-Founder</p>

                  <span className="text-2xl mx-3">|</span>

                  <p className="text-2xl font-bold">Rooted</p>
                </div>

                <i className="text-4xl font-semibold">
                  "It's extremely exciting that Fiverr has freelancers from all
                  over the world — it broadens the talent pool. One of the best
                  things about Fiverr is that while we're sleeping, someone's
                  working."
                </i>
              </div>
            </div>
          </div>

          <div className="slider_item">
            <div className="grid grid-cols-12">
              <div className="left col-span-5">
                <img
                  src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg"
                  alt=""
                />
              </div>

              <div className="right col-span-7 px-20">
                <div className="flex items-center mb-5">
                  <p className="text-2xl">
                    Brighid Gannon (DNP, PMHNP), Co-Founder
                  </p>

                  <span className="text-2xl mx-3">|</span>

                  <p className="text-2xl font-bold">Lavender</p>
                </div>

                <i className="text-4xl font-semibold">
                  "We used Fiverr for SEO, our logo, website, copy, animated
                  videos — literally everything. It was like working with a
                  human right next to you versus being across the world."
                </i>
              </div>
            </div>
          </div>
        </Slider>
      </div>

      <div className="picture relative px-24 py-20">
        <img
          className="rounded-xl"
          src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_1400,dpr_1.0/v1/attachments/generic_asset/asset/50218c41d277f7d85feeaf3efb4549bd-1599072608122/bg-signup-1400-x1.png"
          alt=""
        />

        <div
          style={{
            top: "200px",
            left: "200px",
            width: "800px",
            height: "100px",
          }}
          className="picture_content absolute text-white"
        >
          <p className="text-4xl font-semibold">
            Find the <i>talent</i> needed to <br /> get your business{" "}
            <i>growing.</i>{" "}
          </p>

          <button className=" text-xl bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-8 mt-8 rounded">
            Get Started
          </button>
        </div>
      </div>
    </div>
  );
}
