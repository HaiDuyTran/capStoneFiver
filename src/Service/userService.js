import { https } from "./configURL";

export let userService = {
  postLogin: (dataLogin) => {
    return https.post("/api/auth/signin", dataLogin);
  },
  postRegister: (dataRegister) => {
    return https.post("/api/auth/signup", dataRegister);
  },
};
