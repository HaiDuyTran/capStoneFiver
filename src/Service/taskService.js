import axios from "axios";
import { https, TOKEN_CYBER } from "./configURL";
import { localStorageServ } from "./localStorageService";

export const taskService = {
  getTaskListByName: (tenCongViec) => {
    return https.get(
      `/api/cong-viec/lay-danh-sach-cong-viec-theo-ten/${tenCongViec}`
    );
  },

  getTaskListByType: (maChiTietLoai) => {
    return https.get(
      `/api/cong-viec/lay-cong-viec-theo-chi-tiet-loai/${maChiTietLoai}`
    );
  },

  getTypeOfTask: () => {
    return https.get(`/api/cong-viec/lay-menu-loai-cong-viec`);
  },

  getDetailTypeOfTask: (maLoaiCongViec) => {
    return https.get(
      `/api/cong-viec/lay-chi-tiet-loai-cong-viec/${maLoaiCongViec}`
    );
  },

  getTaskSelected: (maCongViec) => {
    return https.get(`/api/cong-viec/lay-cong-viec-chi-tiet/${maCongViec}`);
  },

  getCommentByTask: (maCongViec) => {
    return https.get(
      `/api/binh-luan/lay-binh-luan-theo-cong-viec/${maCongViec}`
    );
  },

  postComment: (dataComment) => {
    return axios({
      url: "https://fiverrnew.cybersoft.edu.vn/api/binh-luan",
      method: "POST",
      data: dataComment,
      headers: {
        token: localStorageServ.user.get().token,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  rentTask: (dataThueCongViec) => {
    return axios({
      url: "https://fiverrnew.cybersoft.edu.vn/api/thue-cong-viec",
      method: "POST",
      data: dataThueCongViec,
      headers: {
        token: localStorageServ.user.get().token,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },
};
