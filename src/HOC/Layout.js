import React from "react";
import FooterTheme from "../Component/FooterTheme/FooterTheme";
import HeaderTheme from "../Component/HeaderTheme/HeaderTheme";

export default function Layout({ Component }) {
  return (
    <div>
      <HeaderTheme />
      <Component />
      <FooterTheme />
    </div>
  );
}
