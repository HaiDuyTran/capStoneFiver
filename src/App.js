import "./App.css";
import HomePage from "./Page/HomePage/HomePage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LoginPage from "./Page/LoginPage/LoginPage";
import ListTaskPage from "./Page/ListTaskPage/ListTaskPage";
import DetailTaskPage from "./Page/DetailTaskPage/DetailTaskPage";
import Layout from "./HOC/Layout";
import TypeOfTaskPage from "./Page/TypeOfTaskPage/TypeOfTaskPage";
import UserInforPage from "./Page/UserInforPage/UserInforPage";
import RegisterPage from "./Page/RegisterPage/RegisterPage";
import AdminLayout from "./HOC/AdminLayout";
import UserManagement from "./Page/Admin/UserManagement";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          {/* --------------------------------------------USER------------------------------------------------- */}
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route
            path="/taskList/:tenCongViec"
            element={<Layout Component={ListTaskPage} />}
          />
          <Route
            path="/taskList/categories/:maChiTietLoai"
            element={<Layout Component={ListTaskPage} />}
          />
          <Route
            path="/typeOfTask/:maLoaiCongViec"
            element={<Layout Component={TypeOfTaskPage} />}
          />

          <Route
            path="/detailTask/:id"
            element={<Layout Component={DetailTaskPage} />}
          />
          <Route path="/user" element={<Layout Component={UserInforPage} />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />
          {/* --------------------------------------------ADMIN------------------------------------------------- */}

          <Route
            path="/admin"
            element={<AdminLayout Component={UserManagement} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
